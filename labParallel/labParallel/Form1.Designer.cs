﻿namespace labParallel
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edDirTemp = new System.Windows.Forms.TextBox();
            this.buCreateFiles = new System.Windows.Forms.Button();
            this.buDeleteFiles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.edDirTemp.Location = new System.Drawing.Point(13, 13);
            this.edDirTemp.Name = "textBox1";
            this.edDirTemp.Size = new System.Drawing.Size(428, 20);
            this.edDirTemp.TabIndex = 0;
            // 
            // button1
            // 
            this.buCreateFiles.Location = new System.Drawing.Point(13, 40);
            this.buCreateFiles.Name = "button1";
            this.buCreateFiles.Size = new System.Drawing.Size(75, 23);
            this.buCreateFiles.TabIndex = 1;
            this.buCreateFiles.Text = "Create";
            this.buCreateFiles.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buDeleteFiles.Location = new System.Drawing.Point(94, 39);
            this.buDeleteFiles.Name = "button2";
            this.buDeleteFiles.Size = new System.Drawing.Size(75, 23);
            this.buDeleteFiles.TabIndex = 2;
            this.buDeleteFiles.Text = "Delete";
            this.buDeleteFiles.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 393);
            this.Controls.Add(this.buDeleteFiles);
            this.Controls.Add(this.buCreateFiles);
            this.Controls.Add(this.edDirTemp);
            this.Name = "Form1";
            this.Text = "LabParallel";
//            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edDirTemp;
        private System.Windows.Forms.Button buCreateFiles;
        private System.Windows.Forms.Button buDeleteFiles;
    }
}

