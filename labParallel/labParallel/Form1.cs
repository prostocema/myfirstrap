﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace labParallel
{
    public partial class Form1 : Form
    {
        private int countfiles;


        public Form1()
        {
            InitializeComponent();

            edDirTemp.Text = Path.Combine(Path.GetTempPath(), Application.ProductName);
            Directory.CreateDirectory(edDirTemp.Text);

            buCreateFiles.Click += BuCreateFiles_Click;
            buDeleteFiles.Click += buDeleteFiles_Click;
        }

        private void buDeleteFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, countfiles, CreateFile);
            File.Delete(Path.Combine(edDirTemp.Text, $"фаиль{v}.txt"));
        }

        private void BuCreateFiles_Click(object sender, EventArgs e)
        {
            Parallel.For(0, countfiles, CreateFile);
        }

        private void CreateFile(int v)
        {
            File.Create(Path.Combine(edDirTemp.Text, $"фаиль{v}.txt")).Close();
        }
    }
}
