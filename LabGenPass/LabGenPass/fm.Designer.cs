﻿namespace LabGenPass
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Generaciya = new System.Windows.Forms.Button();
            this.checkdown = new System.Windows.Forms.CheckBox();
            this.checkup = new System.Windows.Forms.CheckBox();
            this.numbers = new System.Windows.Forms.CheckBox();
            this.specsym = new System.Windows.Forms.CheckBox();
            this.dlina = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dlina)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Info;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(137, 53);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(449, 64);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "<password>";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Generaciya
            // 
            this.Generaciya.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Generaciya.Location = new System.Drawing.Point(289, 135);
            this.Generaciya.Name = "Generaciya";
            this.Generaciya.Size = new System.Drawing.Size(159, 33);
            this.Generaciya.TabIndex = 1;
            this.Generaciya.Text = "Generaciya";
            this.Generaciya.UseVisualStyleBackColor = false;
            // 
            // checkdown
            // 
            this.checkdown.AutoSize = true;
            this.checkdown.Location = new System.Drawing.Point(112, 189);
            this.checkdown.Name = "checkdown";
            this.checkdown.Size = new System.Drawing.Size(210, 21);
            this.checkdown.TabIndex = 2;
            this.checkdown.Text = "Символы нижнего регистра";
            this.checkdown.UseVisualStyleBackColor = true;
            // 
            // checkup
            // 
            this.checkup.AutoSize = true;
            this.checkup.Location = new System.Drawing.Point(112, 216);
            this.checkup.Name = "checkup";
            this.checkup.Size = new System.Drawing.Size(214, 21);
            this.checkup.TabIndex = 3;
            this.checkup.Text = "Символы верхнего регистра";
            this.checkup.UseVisualStyleBackColor = true;
            // 
            // numbers
            // 
            this.numbers.AutoSize = true;
            this.numbers.Location = new System.Drawing.Point(372, 189);
            this.numbers.Name = "numbers";
            this.numbers.Size = new System.Drawing.Size(78, 21);
            this.numbers.TabIndex = 4;
            this.numbers.Text = "Цифры";
            this.numbers.UseVisualStyleBackColor = true;
            // 
            // specsym
            // 
            this.specsym.AutoSize = true;
            this.specsym.Location = new System.Drawing.Point(372, 216);
            this.specsym.Name = "specsym";
            this.specsym.Size = new System.Drawing.Size(128, 21);
            this.specsym.TabIndex = 5;
            this.specsym.Text = "Спец. символы";
            this.specsym.UseVisualStyleBackColor = true;
            // 
            // dlina
            // 
            this.dlina.Location = new System.Drawing.Point(535, 215);
            this.dlina.Name = "dlina";
            this.dlina.Size = new System.Drawing.Size(120, 22);
            this.dlina.TabIndex = 6;
            this.dlina.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(566, 190);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Длина";
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(728, 277);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dlina);
            this.Controls.Add(this.specsym);
            this.Controls.Add(this.numbers);
            this.Controls.Add(this.checkup);
            this.Controls.Add(this.checkdown);
            this.Controls.Add(this.Generaciya);
            this.Controls.Add(this.textBox1);
            this.Name = "fm";
            this.Text = "GenPass";
            ((System.ComponentModel.ISupportInitialize)(this.dlina)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Generaciya;
        private System.Windows.Forms.CheckBox checkdown;
        private System.Windows.Forms.CheckBox checkup;
        private System.Windows.Forms.CheckBox numbers;
        private System.Windows.Forms.CheckBox specsym;
        private System.Windows.Forms.NumericUpDown dlina;
        private System.Windows.Forms.Label label1;
    }
}

