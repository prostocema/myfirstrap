﻿using System;
using System.Text;

namespace LabGenPasss
{
    class Program
    {
        static void Main(string[] args)
        {
            int pass;
            Console.WriteLine("Укажи количество символов в пароле:");
            pass = int.Parse(Console.ReadLine());
            string result = RandomStr(pass, true, true, true, true);
            Console.WriteLine(result);
        }

        public static string RandomStr(int aLenght, bool aLower, bool aUpper, bool aNumber, bool aSpecl)
        {
            string c1 = "abcdefghigklfnopqrstuvwxyz";
            string c11 = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            string c2 = "0123456789";
            string c3 = "[]{}<>,.;:-+#";

            var x = new StringBuilder(); //nabor symb
            var xResult = new StringBuilder();
            Random rnd = new Random();

            // Создаем набор символов в соответствии с параметрами функции
            if (aLower) x.Append(c1);
            if (aUpper) x.Append(c1.ToUpper());
            if (aNumber) x.Append(c2);
            if (aSpecl) x.Append(c3);
            // Если набор остался пустым, то по умолчанию включаем символы нижнего регистра
            if (x.ToString() == "") x.Append(c1);

            while (xResult.Length < aLenght)
                xResult.Append(x[rnd.Next(x.Length)]);

            return xResult.ToString();
        }
    }
}
