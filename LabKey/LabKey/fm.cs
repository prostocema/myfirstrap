﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabKey
{
    public partial class fm : Form
    {
        public fm()
        {
            InitializeComponent();

            this.KeyDown += Fm_KeyDown;
        }

        private void Fm_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    lbl.Text = "Left";
                    break;
                case Keys.Right:
                    lbl.Text = "Right";
                    break;
                case Keys.Up:
                    lbl.Text = "Up";
                    break;
                case Keys.Down:
                    lbl.Text = "Down";
                    break;
                case Keys.Space:
                    if (e.Shift)
                    {
                        lbl.Text = "Shift + Space";
                    }
                    else
                    {
                        lbl.Text = "Space";
                    }
                    break;
                case Keys.Z:
                    lbl.Text = e.Shift ? "Shift + Z" : "Z";
                    break;
                default:
                    lbl.Text = e.KeyCode.ToString();
                    break;
            }
        }
    }
}
