﻿namespace LabSQLLite
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvLogs = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.edSQL = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buUsersShow = new System.Windows.Forms.Button();
            this.buNotesShow = new System.Windows.Forms.Button();
            this.buRunOne = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.edNotesCaption = new System.Windows.Forms.TextBox();
            this.edNotesPriority = new System.Windows.Forms.NumericUpDown();
            this.buNotesAdd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPriority)).BeginInit();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.lvLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lvLogs.HideSelection = false;
            this.lvLogs.Location = new System.Drawing.Point(12, 42);
            this.lvLogs.Name = "listView1";
            this.lvLogs.Size = new System.Drawing.Size(168, 357);
            this.lvLogs.TabIndex = 0;
            this.lvLogs.UseCompatibleStateImageBehavior = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Лог запусков приложения";
            // 
            // textBox1
            // 
            this.edSQL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.edSQL.Location = new System.Drawing.Point(186, 42);
            this.edSQL.Multiline = true;
            this.edSQL.Name = "textBox1";
            this.edSQL.Size = new System.Drawing.Size(503, 50);
            this.edSQL.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(187, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "SQL";
            // 
            // button1
            // 
            this.buUsersShow.Location = new System.Drawing.Point(186, 99);
            this.buUsersShow.Name = "button1";
            this.buUsersShow.Size = new System.Drawing.Size(109, 28);
            this.buUsersShow.TabIndex = 4;
            this.buUsersShow.Text = "Пользаки";
            this.buUsersShow.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.buNotesShow.Location = new System.Drawing.Point(301, 98);
            this.buNotesShow.Name = "button2";
            this.buNotesShow.Size = new System.Drawing.Size(109, 28);
            this.buNotesShow.TabIndex = 5;
            this.buNotesShow.Text = "Заметки";
            this.buNotesShow.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.buRunOne.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buRunOne.Location = new System.Drawing.Point(465, 98);
            this.buRunOne.Name = "button3";
            this.buRunOne.Size = new System.Drawing.Size(109, 28);
            this.buRunOne.TabIndex = 6;
            this.buRunOne.Text = "Выполнить";
            this.buRunOne.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(580, 99);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(109, 28);
            this.button4.TabIndex = 7;
            this.button4.Text = "Выполнить Sql";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(186, 134);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(503, 202);
            this.dataGridView1.TabIndex = 8;
            // 
            // textBox2
            // 
            this.edNotesCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edNotesCaption.Location = new System.Drawing.Point(186, 379);
            this.edNotesCaption.Name = "textBox2";
            this.edNotesCaption.Size = new System.Drawing.Size(165, 20);
            this.edNotesCaption.TabIndex = 9;
            // 
            // numericUpDown1
            // 
            this.edNotesPriority.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.edNotesPriority.Location = new System.Drawing.Point(357, 380);
            this.edNotesPriority.Name = "numericUpDown1";
            this.edNotesPriority.Size = new System.Drawing.Size(120, 20);
            this.edNotesPriority.TabIndex = 10;
            // 
            // button5
            // 
            this.buNotesAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buNotesAdd.Location = new System.Drawing.Point(483, 380);
            this.buNotesAdd.Name = "button5";
            this.buNotesAdd.Size = new System.Drawing.Size(75, 23);
            this.buNotesAdd.TabIndex = 11;
            this.buNotesAdd.Text = "+заметка";
            this.buNotesAdd.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 411);
            this.Controls.Add(this.buNotesAdd);
            this.Controls.Add(this.edNotesPriority);
            this.Controls.Add(this.edNotesCaption);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buRunOne);
            this.Controls.Add(this.buNotesShow);
            this.Controls.Add(this.buUsersShow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edSQL);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvLogs);
            this.Name = "Form1";
            this.Text = "Form1";
//            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edNotesPriority)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvLogs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edSQL;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buUsersShow;
        private System.Windows.Forms.Button buNotesShow;
        private System.Windows.Forms.Button buRunOne;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox edNotesCaption;
        private System.Windows.Forms.NumericUpDown edNotesPriority;
        private System.Windows.Forms.Button buNotesAdd;
    }
}

