﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fifteen
{
    public partial class fifteen : Form
    {
        Game game;
        int count = 0;
        public fifteen()
        {
            InitializeComponent();
            game = new Game(4);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            count++;
            int position = Convert.ToInt16(((Button)sender).Tag);
            game.shift(position);
            refresh();
            if (game.check_numbers())
            {
                MessageBox.Show("Игра окончена!", "Congarulations!!");
                start_game();
                count = 0;
            }
            clickcount.Text = count.ToString();
        }

        private Button button(int position)
        {
            switch (position)
            {
                case 0: return button0;
                case 1: return button1;
                case 2: return button2;
                case 3: return button3;
                case 4: return button4;
                case 5: return button5;
                case 6: return button6;
                case 7: return button7;
                case 8: return button8;
                case 9: return button9;
                case 10: return button10;
                case 11: return button11;
                case 12: return button12;
                case 13: return button13;
                case 14: return button14;
                case 15: return button15;

                default: return null;
            }
        }

  /*      private void start_Click(object sender, EventArgs e)
        {
            start_game();
        } */
        private void fifteen_Load(object sender, EventArgs e)
        {
            //    start_game();
            MessageBox.Show("Добро пожаловать в игру пятнашки. \n" +
                "Выберите уровень сложности или введите вручную сколько перемешиваний будет в игре,\n" +
                " а затем перемещайте фишки так, что-бы итоговая " +
                "картина сложилась в ряд от 1 до 15.", "Предысловие");
        }
        private void start_game()
        {
            game.start();
            for (int i = 0; i < 10; i++)
                game.shift_smesh();
            refresh();
        }

        private void refresh ()
        {
            for (int position = 0; position < 16; position++)
            {
                int nr = game.get_number(position);
                button(position).Text = game.get_number(position).ToString();
                button(position).Visible = (nr > 0);
            }
        }

        private void easy_Click(object sender, EventArgs e)
        {
            game.start();
            for (int i = 0; i < 20; i++)
                game.shift_smesh();
            refresh();
        }

        private void medium_Click(object sender, EventArgs e)
        {
            game.start();
            for (int i = 0; i < 50; i++)
                game.shift_smesh();
            refresh();
        }

        private void hard_Click(object sender, EventArgs e)
        {
            game.start();
            for (int i = 0; i < 100; i++)
                game.shift_smesh();
            refresh();
        }

        private void button16_Click_1(object sender, EventArgs e)
        {
            int j = 0;
            try
            {
                j = Convert.ToInt16(countmesh.Text);
                countmesh.Text = "";
                game.start();
                for (int i = 0; i < j; i++)
                    game.shift_smesh();
                refresh();
            }
            catch
            {
                MessageBox.Show("Аккуратнее с вводимыми символами.", "Предупреждение.");
            }
        }
    }
}
