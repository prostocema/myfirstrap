﻿namespace Fifteen
{
    partial class fifteen
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.level = new System.Windows.Forms.ToolStripMenuItem();
            this.easy = new System.Windows.Forms.ToolStripMenuItem();
            this.medium = new System.Windows.Forms.ToolStripMenuItem();
            this.hard = new System.Windows.Forms.ToolStripMenuItem();
            this.clickcount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.countmesh = new System.Windows.Forms.TextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.OldLace;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.button15, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.button14, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.button13, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.button12, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.button11, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.button10, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.button9, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button7, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.button6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.button5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.button2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button0, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(553, 399);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.LightYellow;
            this.button15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button15.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button15.Location = new System.Drawing.Point(424, 307);
            this.button15.Margin = new System.Windows.Forms.Padding(10);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(119, 82);
            this.button15.TabIndex = 15;
            this.button15.Tag = "15";
            this.button15.Text = "-";
            this.button15.UseVisualStyleBackColor = false;
            this.button15.Click += new System.EventHandler(this.button16_Click);
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.LightYellow;
            this.button14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button14.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button14.Location = new System.Drawing.Point(286, 307);
            this.button14.Margin = new System.Windows.Forms.Padding(10);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(118, 82);
            this.button14.TabIndex = 14;
            this.button14.Tag = "14";
            this.button14.Text = "-";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button16_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.LightYellow;
            this.button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button13.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button13.Location = new System.Drawing.Point(148, 307);
            this.button13.Margin = new System.Windows.Forms.Padding(10);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(118, 82);
            this.button13.TabIndex = 13;
            this.button13.Tag = "13";
            this.button13.Text = "-";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button16_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.LightYellow;
            this.button12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button12.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button12.Location = new System.Drawing.Point(10, 307);
            this.button12.Margin = new System.Windows.Forms.Padding(10);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(118, 82);
            this.button12.TabIndex = 12;
            this.button12.Tag = "12";
            this.button12.Text = "-";
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button16_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.LightYellow;
            this.button11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button11.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button11.Location = new System.Drawing.Point(424, 208);
            this.button11.Margin = new System.Windows.Forms.Padding(10);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(119, 79);
            this.button11.TabIndex = 11;
            this.button11.Tag = "11";
            this.button11.Text = "-";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.button16_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.LightYellow;
            this.button10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button10.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button10.Location = new System.Drawing.Point(286, 208);
            this.button10.Margin = new System.Windows.Forms.Padding(10);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(118, 79);
            this.button10.TabIndex = 10;
            this.button10.Tag = "10";
            this.button10.Text = "-";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button16_Click);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.LightYellow;
            this.button9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button9.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button9.Location = new System.Drawing.Point(148, 208);
            this.button9.Margin = new System.Windows.Forms.Padding(10);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(118, 79);
            this.button9.TabIndex = 9;
            this.button9.Tag = "9";
            this.button9.Text = "-";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button16_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.LightYellow;
            this.button8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button8.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button8.Location = new System.Drawing.Point(10, 208);
            this.button8.Margin = new System.Windows.Forms.Padding(10);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(118, 79);
            this.button8.TabIndex = 8;
            this.button8.Tag = "8";
            this.button8.Text = "-";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button16_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.LightYellow;
            this.button7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button7.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button7.Location = new System.Drawing.Point(424, 109);
            this.button7.Margin = new System.Windows.Forms.Padding(10);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(119, 79);
            this.button7.TabIndex = 7;
            this.button7.Tag = "7";
            this.button7.Text = "-";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button16_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.LightYellow;
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button6.Location = new System.Drawing.Point(286, 109);
            this.button6.Margin = new System.Windows.Forms.Padding(10);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(118, 79);
            this.button6.TabIndex = 6;
            this.button6.Tag = "6";
            this.button6.Text = "-";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button16_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.LightYellow;
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button5.Location = new System.Drawing.Point(148, 109);
            this.button5.Margin = new System.Windows.Forms.Padding(10);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(118, 79);
            this.button5.TabIndex = 5;
            this.button5.Tag = "5";
            this.button5.Text = "-";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button16_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.LightYellow;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button4.Location = new System.Drawing.Point(10, 109);
            this.button4.Margin = new System.Windows.Forms.Padding(10);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(118, 79);
            this.button4.TabIndex = 4;
            this.button4.Tag = "4";
            this.button4.Text = "-";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button16_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightYellow;
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button3.Location = new System.Drawing.Point(424, 10);
            this.button3.Margin = new System.Windows.Forms.Padding(10);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 79);
            this.button3.TabIndex = 3;
            this.button3.Tag = "3";
            this.button3.Text = "-";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button16_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LightYellow;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button2.Location = new System.Drawing.Point(286, 10);
            this.button2.Margin = new System.Windows.Forms.Padding(10);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(118, 79);
            this.button2.TabIndex = 2;
            this.button2.Tag = "2";
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button16_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightYellow;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Impact", 24.75F);
            this.button1.Location = new System.Drawing.Point(148, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 79);
            this.button1.TabIndex = 1;
            this.button1.Tag = "1";
            this.button1.Text = "-";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button16_Click);
            // 
            // button0
            // 
            this.button0.BackColor = System.Drawing.Color.LightYellow;
            this.button0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button0.Font = new System.Drawing.Font("Impact", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button0.Location = new System.Drawing.Point(10, 10);
            this.button0.Margin = new System.Windows.Forms.Padding(10);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(118, 79);
            this.button0.TabIndex = 0;
            this.button0.Tag = "0";
            this.button0.Text = "-";
            this.button0.UseVisualStyleBackColor = false;
            this.button0.Click += new System.EventHandler(this.button16_Click);
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.level});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(553, 24);
            this.menu.TabIndex = 1;
            this.menu.Text = "menuStrip1";
            // 
            // level
            // 
            this.level.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.easy,
            this.medium,
            this.hard});
            this.level.Name = "level";
            this.level.Size = new System.Drawing.Size(119, 20);
            this.level.Text = "Уровень сложности";
            // 
            // easy
            // 
            this.easy.Name = "easy";
            this.easy.Size = new System.Drawing.Size(180, 22);
            this.easy.Text = "Лёгкий";
            this.easy.Click += new System.EventHandler(this.easy_Click);
            // 
            // medium
            // 
            this.medium.Name = "medium";
            this.medium.Size = new System.Drawing.Size(180, 22);
            this.medium.Text = "Средний";
            this.medium.Click += new System.EventHandler(this.medium_Click);
            // 
            // hard
            // 
            this.hard.Name = "hard";
            this.hard.Size = new System.Drawing.Size(180, 22);
            this.hard.Text = "Сложный";
            this.hard.Click += new System.EventHandler(this.hard_Click);
            // 
            // clickcount
            // 
            this.clickcount.AutoSize = true;
            this.clickcount.Location = new System.Drawing.Point(528, 8);
            this.clickcount.Name = "clickcount";
            this.clickcount.Size = new System.Drawing.Size(13, 13);
            this.clickcount.TabIndex = 2;
            this.clickcount.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(421, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Количество ходов:";
            // 
            // countmesh
            // 
            this.countmesh.Location = new System.Drawing.Point(209, 1);
            this.countmesh.Name = "countmesh";
            this.countmesh.Size = new System.Drawing.Size(100, 20);
            this.countmesh.TabIndex = 4;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(315, -1);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(82, 23);
            this.button16.TabIndex = 5;
            this.button16.Text = "Перемешать";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click_1);
            // 
            // fifteen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 423);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.countmesh);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.clickcount);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menu);
            this.MainMenuStrip = this.menu;
            this.Name = "fifteen";
            this.Text = "Fifteen";
            this.Load += new System.EventHandler(this.fifteen_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem level;
        private System.Windows.Forms.Label clickcount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem easy;
        private System.Windows.Forms.ToolStripMenuItem medium;
        private System.Windows.Forms.ToolStripMenuItem hard;
        private System.Windows.Forms.TextBox countmesh;
        private System.Windows.Forms.Button button16;
    }
}

