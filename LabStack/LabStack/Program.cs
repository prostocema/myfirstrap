﻿using System;
using System.Collections;

namespace LabStack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack x = new Stack();
            x.Push("Рязань");
            x.Push(123);
            x.Push("Tula");

            Console.WriteLine(x.Peek());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());

            #region try-catch

            try
            {
                Console.WriteLine(x.Pop());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            #endregion
        }
    }
}
