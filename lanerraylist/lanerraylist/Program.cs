﻿using System;
using System.Collections;

namespace lanerraylist
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add(3.14);
            list.Add(256);
            list.AddRange(new string[] { "Hello", "world" });

            foreach (object o in list)
            {
                Console.WriteLine(o);
            }
            Console.WriteLine();
            Console.WriteLine("zerkalo");
            Console.WriteLine();
            //   list.RemoveAt(0); // delete 0 element
            list.Reverse();
            //Console.WriteLine(list[0]); po indexu

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
            Console.ReadLine();
        }
    }
}
