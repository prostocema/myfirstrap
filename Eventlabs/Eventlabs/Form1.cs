﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Eventlabs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            // 2
            button2.Click += button2_Click;
            // 3
            button3.Click += delegate
            {
                MessageBox.Show("asdasd");
            };
            //4
            button4.Click += (sender, e) => MessageBox.Show("qweqweqwe");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("УРА");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Sposobssss");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ////////
        }
    }
}
