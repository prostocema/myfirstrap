﻿namespace WebBrowserLab
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.url = new System.Windows.Forms.TextBox();
            this.enter = new System.Windows.Forms.Button();
            this.webbro = new System.Windows.Forms.WebBrowser();
            this.stop = new System.Windows.Forms.Button();
            this.reload = new System.Windows.Forms.Button();
            this.vpered = new System.Windows.Forms.Button();
            this.nazad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // url
            // 
            this.url.Location = new System.Drawing.Point(13, 13);
            this.url.Name = "url";
            this.url.Size = new System.Drawing.Size(457, 22);
            this.url.TabIndex = 0;
            this.url.Text = "http://";
            this.url.TextChanged += new System.EventHandler(this.url_TextChanged);
            // 
            // enter
            // 
            this.enter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.enter.Location = new System.Drawing.Point(475, 13);
            this.enter.Name = "enter";
            this.enter.Size = new System.Drawing.Size(137, 23);
            this.enter.TabIndex = 1;
            this.enter.Text = "Enter";
            this.enter.UseVisualStyleBackColor = true;
            // 
            // webbro
            // 
            this.webbro.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webbro.Location = new System.Drawing.Point(13, 42);
            this.webbro.MinimumSize = new System.Drawing.Size(20, 20);
            this.webbro.Name = "webbro";
            this.webbro.Size = new System.Drawing.Size(722, 473);
            this.webbro.TabIndex = 2;
            // 
            // stop
            // 
            this.stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.stop.Location = new System.Drawing.Point(661, 531);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(75, 31);
            this.stop.TabIndex = 3;
            this.stop.Text = "stopppp";
            this.stop.UseVisualStyleBackColor = true;
            // 
            // reload
            // 
            this.reload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.reload.Location = new System.Drawing.Point(619, 12);
            this.reload.Name = "reload";
            this.reload.Size = new System.Drawing.Size(75, 24);
            this.reload.TabIndex = 4;
            this.reload.Text = "reload";
            this.reload.UseVisualStyleBackColor = true;
            // 
            // vpered
            // 
            this.vpered.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.vpered.Location = new System.Drawing.Point(94, 531);
            this.vpered.Name = "vpered";
            this.vpered.Size = new System.Drawing.Size(75, 31);
            this.vpered.TabIndex = 5;
            this.vpered.Text = "vpered";
            this.vpered.UseVisualStyleBackColor = true;
            // 
            // nazad
            // 
            this.nazad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nazad.Location = new System.Drawing.Point(13, 531);
            this.nazad.Name = "nazad";
            this.nazad.Size = new System.Drawing.Size(75, 31);
            this.nazad.TabIndex = 6;
            this.nazad.Text = "nazad";
            this.nazad.UseVisualStyleBackColor = true;
            this.nazad.Click += new System.EventHandler(this.button5_Click);
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 574);
            this.Controls.Add(this.nazad);
            this.Controls.Add(this.vpered);
            this.Controls.Add(this.reload);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.webbro);
            this.Controls.Add(this.enter);
            this.Controls.Add(this.url);
            this.Name = "fm";
            this.Text = "WEBBrowserLab";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox url;
        private System.Windows.Forms.Button enter;
        private System.Windows.Forms.WebBrowser webbro;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button reload;
        private System.Windows.Forms.Button vpered;
        private System.Windows.Forms.Button nazad;
    }
}

