﻿namespace infinitepolytech
{
    partial class fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fm));
            this.rule = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.electro = new System.Windows.Forms.PictureBox();
            this.speaker = new System.Windows.Forms.PictureBox();
            this.yes = new System.Windows.Forms.Button();
            this.no = new System.Windows.Forms.Button();
            this.wowow = new System.Windows.Forms.Label();
            this.wowow2 = new System.Windows.Forms.Label();
            this.conf = new System.Windows.Forms.PictureBox();
            this.coolroom = new System.Windows.Forms.PictureBox();
            this.wau = new System.Windows.Forms.Button();
            this.ladno = new System.Windows.Forms.Button();
            this.dddmod = new System.Windows.Forms.PictureBox();
            this.endec = new System.Windows.Forms.Button();
            this.more = new System.Windows.Forms.Button();
            this.vicyes = new System.Windows.Forms.Button();
            this.vicno = new System.Windows.Forms.Button();
            this.poka = new System.Windows.Forms.Button();
            this.dialog = new System.Windows.Forms.Label();
            this.speakervic = new System.Windows.Forms.PictureBox();
            this.question = new System.Windows.Forms.Label();
            this.polojitelno = new System.Windows.Forms.Button();
            this.otricatelno = new System.Windows.Forms.Button();
            this.demo = new System.Windows.Forms.Label();
            this.two = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.nocount = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.next = new System.Windows.Forms.Button();
            this.byebye = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.electro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speaker)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.conf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coolroom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dddmod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.speakervic)).BeginInit();
            this.SuspendLayout();
            // 
            // rule
            // 
            this.rule.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.rule.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rule.Location = new System.Drawing.Point(321, 188);
            this.rule.Name = "rule";
            this.rule.Size = new System.Drawing.Size(238, 58);
            this.rule.TabIndex = 0;
            this.rule.Text = "Правила";
            this.rule.UseVisualStyleBackColor = true;
            this.rule.Click += new System.EventHandler(this.rule_Click);
            // 
            // start
            // 
            this.start.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.start.Location = new System.Drawing.Point(321, 102);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(238, 58);
            this.start.TabIndex = 1;
            this.start.Text = "Начать игру";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // exit
            // 
            this.exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.exit.Location = new System.Drawing.Point(321, 274);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(238, 58);
            this.exit.TabIndex = 2;
            this.exit.Text = "Выход";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // electro
            // 
            this.electro.Image = ((System.Drawing.Image)(resources.GetObject("electro.Image")));
            this.electro.Location = new System.Drawing.Point(87, 27);
            this.electro.Name = "electro";
            this.electro.Size = new System.Drawing.Size(693, 390);
            this.electro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.electro.TabIndex = 3;
            this.electro.TabStop = false;
            // 
            // speaker
            // 
            this.speaker.Image = ((System.Drawing.Image)(resources.GetObject("speaker.Image")));
            this.speaker.Location = new System.Drawing.Point(777, 132);
            this.speaker.Name = "speaker";
            this.speaker.Size = new System.Drawing.Size(111, 233);
            this.speaker.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.speaker.TabIndex = 4;
            this.speaker.TabStop = false;
            this.speaker.MouseLeave += new System.EventHandler(this.speaker_MouseLeave);
            this.speaker.MouseHover += new System.EventHandler(this.speaker_MouseHover);
            // 
            // yes
            // 
            this.yes.Location = new System.Drawing.Point(786, 364);
            this.yes.Name = "yes";
            this.yes.Size = new System.Drawing.Size(88, 23);
            this.yes.TabIndex = 6;
            this.yes.Text = "Да, конечно";
            this.yes.UseVisualStyleBackColor = true;
            this.yes.Click += new System.EventHandler(this.yes_Click);
            // 
            // no
            // 
            this.no.Location = new System.Drawing.Point(787, 394);
            this.no.Name = "no";
            this.no.Size = new System.Drawing.Size(87, 23);
            this.no.TabIndex = 7;
            this.no.Text = "Нет, конечно";
            this.no.UseVisualStyleBackColor = true;
            this.no.Click += new System.EventHandler(this.no_Click);
            // 
            // wowow
            // 
            this.wowow.AutoSize = true;
            this.wowow.ForeColor = System.Drawing.Color.White;
            this.wowow.Location = new System.Drawing.Point(787, 91);
            this.wowow.Name = "wowow";
            this.wowow.Size = new System.Drawing.Size(0, 13);
            this.wowow.TabIndex = 8;
            this.wowow.MouseEnter += new System.EventHandler(this.speaker_MouseLeave);
            this.wowow.MouseLeave += new System.EventHandler(this.speaker_MouseLeave);
            // 
            // wowow2
            // 
            this.wowow2.AutoSize = true;
            this.wowow2.ForeColor = System.Drawing.Color.White;
            this.wowow2.Location = new System.Drawing.Point(787, 102);
            this.wowow2.Name = "wowow2";
            this.wowow2.Size = new System.Drawing.Size(0, 13);
            this.wowow2.TabIndex = 9;
            // 
            // conf
            // 
            this.conf.Image = ((System.Drawing.Image)(resources.GetObject("conf.Image")));
            this.conf.Location = new System.Drawing.Point(55, 27);
            this.conf.Name = "conf";
            this.conf.Size = new System.Drawing.Size(725, 390);
            this.conf.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.conf.TabIndex = 10;
            this.conf.TabStop = false;
            // 
            // coolroom
            // 
            this.coolroom.Image = ((System.Drawing.Image)(resources.GetObject("coolroom.Image")));
            this.coolroom.Location = new System.Drawing.Point(37, 27);
            this.coolroom.Name = "coolroom";
            this.coolroom.Size = new System.Drawing.Size(743, 390);
            this.coolroom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.coolroom.TabIndex = 12;
            this.coolroom.TabStop = false;
            // 
            // wau
            // 
            this.wau.Location = new System.Drawing.Point(787, 364);
            this.wau.Name = "wau";
            this.wau.Size = new System.Drawing.Size(87, 23);
            this.wau.TabIndex = 13;
            this.wau.Text = "Вау..";
            this.wau.UseVisualStyleBackColor = true;
            this.wau.Click += new System.EventHandler(this.wau_Click);
            // 
            // ladno
            // 
            this.ladno.Location = new System.Drawing.Point(786, 364);
            this.ladno.Name = "ladno";
            this.ladno.Size = new System.Drawing.Size(88, 23);
            this.ladno.TabIndex = 14;
            this.ladno.Text = "Ладно..";
            this.ladno.UseVisualStyleBackColor = true;
            this.ladno.Click += new System.EventHandler(this.ladno_Click);
            // 
            // dddmod
            // 
            this.dddmod.Image = ((System.Drawing.Image)(resources.GetObject("dddmod.Image")));
            this.dddmod.Location = new System.Drawing.Point(28, 27);
            this.dddmod.Name = "dddmod";
            this.dddmod.Size = new System.Drawing.Size(743, 390);
            this.dddmod.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dddmod.TabIndex = 15;
            this.dddmod.TabStop = false;
            // 
            // endec
            // 
            this.endec.Location = new System.Drawing.Point(777, 74);
            this.endec.Name = "endec";
            this.endec.Size = new System.Drawing.Size(113, 46);
            this.endec.TabIndex = 16;
            this.endec.Text = "Можно завершить экскурсию?";
            this.endec.UseVisualStyleBackColor = true;
            this.endec.Click += new System.EventHandler(this.endec_Click);
            // 
            // more
            // 
            this.more.Location = new System.Drawing.Point(787, 364);
            this.more.Name = "more";
            this.more.Size = new System.Drawing.Size(87, 46);
            this.more.TabIndex = 17;
            this.more.Text = "Дальше";
            this.more.UseVisualStyleBackColor = true;
            this.more.Click += new System.EventHandler(this.more_Click);
            // 
            // vicyes
            // 
            this.vicyes.Location = new System.Drawing.Point(786, 347);
            this.vicyes.Name = "vicyes";
            this.vicyes.Size = new System.Drawing.Size(88, 35);
            this.vicyes.TabIndex = 18;
            this.vicyes.Text = "Да, с удовольсвием";
            this.vicyes.UseVisualStyleBackColor = true;
            this.vicyes.Click += new System.EventHandler(this.vicyes_Click);
            // 
            // vicno
            // 
            this.vicno.Location = new System.Drawing.Point(787, 382);
            this.vicno.Name = "vicno";
            this.vicno.Size = new System.Drawing.Size(87, 35);
            this.vicno.TabIndex = 19;
            this.vicno.Text = "Пожалуй, откажусь";
            this.vicno.UseVisualStyleBackColor = true;
            this.vicno.Click += new System.EventHandler(this.vicno_Click);
            // 
            // poka
            // 
            this.poka.Location = new System.Drawing.Point(786, 365);
            this.poka.Name = "poka";
            this.poka.Size = new System.Drawing.Size(88, 23);
            this.poka.TabIndex = 20;
            this.poka.Text = "До свидания!";
            this.poka.UseVisualStyleBackColor = true;
            this.poka.Click += new System.EventHandler(this.poka_Click);
            // 
            // dialog
            // 
            this.dialog.AutoSize = true;
            this.dialog.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dialog.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dialog.Location = new System.Drawing.Point(188, 328);
            this.dialog.Name = "dialog";
            this.dialog.Size = new System.Drawing.Size(0, 24);
            this.dialog.TabIndex = 21;
            // 
            // speakervic
            // 
            this.speakervic.Image = ((System.Drawing.Image)(resources.GetObject("speakervic.Image")));
            this.speakervic.Location = new System.Drawing.Point(305, 51);
            this.speakervic.Name = "speakervic";
            this.speakervic.Size = new System.Drawing.Size(475, 366);
            this.speakervic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.speakervic.TabIndex = 22;
            this.speakervic.TabStop = false;
            // 
            // question
            // 
            this.question.AutoSize = true;
            this.question.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.question.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.question.Location = new System.Drawing.Point(197, 74);
            this.question.Name = "question";
            this.question.Size = new System.Drawing.Size(0, 24);
            this.question.TabIndex = 23;
            // 
            // polojitelno
            // 
            this.polojitelno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.polojitelno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.polojitelno.ForeColor = System.Drawing.Color.MediumSeaGreen;
            this.polojitelno.Location = new System.Drawing.Point(201, 166);
            this.polojitelno.Name = "polojitelno";
            this.polojitelno.Size = new System.Drawing.Size(175, 46);
            this.polojitelno.TabIndex = 24;
            this.polojitelno.Text = "Положительно";
            this.polojitelno.UseVisualStyleBackColor = false;
            this.polojitelno.Click += new System.EventHandler(this.polojitelno_Click);
            // 
            // otricatelno
            // 
            this.otricatelno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.otricatelno.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.otricatelno.ForeColor = System.Drawing.Color.Red;
            this.otricatelno.Location = new System.Drawing.Point(201, 222);
            this.otricatelno.Name = "otricatelno";
            this.otricatelno.Size = new System.Drawing.Size(175, 46);
            this.otricatelno.TabIndex = 25;
            this.otricatelno.Text = "Отрицательно";
            this.otricatelno.UseVisualStyleBackColor = false;
            this.otricatelno.Click += new System.EventHandler(this.otricatelno_Click);
            // 
            // demo
            // 
            this.demo.AutoSize = true;
            this.demo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.demo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.demo.Location = new System.Drawing.Point(201, 295);
            this.demo.Name = "demo";
            this.demo.Size = new System.Drawing.Size(0, 24);
            this.demo.TabIndex = 26;
            // 
            // two
            // 
            this.two.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.two.Location = new System.Drawing.Point(201, 166);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(50, 36);
            this.two.TabIndex = 27;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.two_Click);
            // 
            // three
            // 
            this.three.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.three.Location = new System.Drawing.Point(275, 166);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(50, 36);
            this.three.TabIndex = 28;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.three_Click);
            // 
            // four
            // 
            this.four.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.four.Location = new System.Drawing.Point(201, 222);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(50, 36);
            this.four.TabIndex = 29;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = true;
            this.four.Click += new System.EventHandler(this.four_Click);
            // 
            // five
            // 
            this.five.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.five.Location = new System.Drawing.Point(275, 222);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(50, 36);
            this.five.TabIndex = 30;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.five_Click);
            // 
            // nocount
            // 
            this.nocount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nocount.Location = new System.Drawing.Point(201, 275);
            this.nocount.Name = "nocount";
            this.nocount.Size = new System.Drawing.Size(124, 34);
            this.nocount.TabIndex = 31;
            this.nocount.Text = "Не считал(";
            this.nocount.UseVisualStyleBackColor = true;
            this.nocount.Click += new System.EventHandler(this.nocount_Click);
            // 
            // back
            // 
            this.back.Location = new System.Drawing.Point(201, 136);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(75, 23);
            this.back.TabIndex = 32;
            this.back.Text = "Вернутся";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // next
            // 
            this.next.Location = new System.Drawing.Point(282, 136);
            this.next.Name = "next";
            this.next.Size = new System.Drawing.Size(94, 23);
            this.next.TabIndex = 33;
            this.next.Text = "Продолжить";
            this.next.UseVisualStyleBackColor = true;
            this.next.Click += new System.EventHandler(this.next_Click);
            // 
            // byebye
            // 
            this.byebye.BackColor = System.Drawing.Color.Black;
            this.byebye.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.byebye.ForeColor = System.Drawing.Color.White;
            this.byebye.Location = new System.Drawing.Point(305, 252);
            this.byebye.Name = "byebye";
            this.byebye.Size = new System.Drawing.Size(228, 35);
            this.byebye.TabIndex = 34;
            this.byebye.Text = "Спасибо. До встречи!";
            this.byebye.UseVisualStyleBackColor = false;
            this.byebye.Click += new System.EventHandler(this.byebye_Click);
            // 
            // fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(893, 418);
            this.Controls.Add(this.byebye);
            this.Controls.Add(this.next);
            this.Controls.Add(this.back);
            this.Controls.Add(this.nocount);
            this.Controls.Add(this.five);
            this.Controls.Add(this.four);
            this.Controls.Add(this.three);
            this.Controls.Add(this.two);
            this.Controls.Add(this.demo);
            this.Controls.Add(this.otricatelno);
            this.Controls.Add(this.polojitelno);
            this.Controls.Add(this.question);
            this.Controls.Add(this.dialog);
            this.Controls.Add(this.speakervic);
            this.Controls.Add(this.poka);
            this.Controls.Add(this.vicno);
            this.Controls.Add(this.vicyes);
            this.Controls.Add(this.more);
            this.Controls.Add(this.endec);
            this.Controls.Add(this.dddmod);
            this.Controls.Add(this.ladno);
            this.Controls.Add(this.wau);
            this.Controls.Add(this.coolroom);
            this.Controls.Add(this.conf);
            this.Controls.Add(this.wowow2);
            this.Controls.Add(this.wowow);
            this.Controls.Add(this.no);
            this.Controls.Add(this.yes);
            this.Controls.Add(this.speaker);
            this.Controls.Add(this.electro);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.start);
            this.Controls.Add(this.rule);
            this.Name = "fm";
            this.Text = "Бесконечный Политех";
            ((System.ComponentModel.ISupportInitialize)(this.electro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speaker)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.conf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coolroom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dddmod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.speakervic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button rule;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.PictureBox electro;
        private System.Windows.Forms.PictureBox speaker;
        private System.Windows.Forms.Button yes;
        private System.Windows.Forms.Button no;
        private System.Windows.Forms.Label wowow;
        private System.Windows.Forms.Label wowow2;
        private System.Windows.Forms.PictureBox conf;
        private System.Windows.Forms.PictureBox coolroom;
        private System.Windows.Forms.Button wau;
        private System.Windows.Forms.Button ladno;
        private System.Windows.Forms.PictureBox dddmod;
        private System.Windows.Forms.Button endec;
        private System.Windows.Forms.Button more;
        private System.Windows.Forms.Button vicyes;
        private System.Windows.Forms.Button vicno;
        private System.Windows.Forms.Button poka;
        private System.Windows.Forms.Label dialog;
        private System.Windows.Forms.PictureBox speakervic;
        private System.Windows.Forms.Label question;
        private System.Windows.Forms.Button polojitelno;
        private System.Windows.Forms.Button otricatelno;
        private System.Windows.Forms.Label demo;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button nocount;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button next;
        private System.Windows.Forms.Button byebye;
    }
}

