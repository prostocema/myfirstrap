﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace infinitepolytech
{
    public partial class fm : Form
    {
        private const bool V = true;
        private const bool V1 = false;
        int non = 0;

        public fm()
        {
            InitializeComponent();
            electro.Visible = V1;
            speaker.Visible = V1;
            yes.Visible = V1;
            no.Visible = V1;
            wowow.Visible = V1;
            conf.Visible = V1;
            coolroom.Visible = V1;
            wau.Visible = V1;
            ladno.Visible = V1;
            dddmod.Visible = V1;
            endec.Visible = V1;
            more.Visible = V1;
            vicyes.Visible = V1;
            vicno.Visible = V1;
            poka.Visible = V1;
            speakervic.Visible = V1;
            question.Visible = V1;
            polojitelno.Visible = V1;
            otricatelno.Visible = V1;
            two.Visible = V1;
            three.Visible = V1;
            four.Visible = V1;
            five.Visible = V1;
            nocount.Visible = V1;
            back.Visible = V1;
            next.Visible = V1;
            byebye.Visible = V1;
            wowow.Text = "Не наводи на меня\n" +
                "курсор. Пожалуйста.";
 //           wowow2.Text = "Я же попросил.";
        }

        private void rule_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Добро пожаловать в игру Бескоченый политех. Правила просты.\n" +
                "Следуй сюжету, отвечая на поставленные вопросы или предложения так, как хочешь ТЫ.");
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void start_Click(object sender, EventArgs e)
        {
            rule.Visible = V1;
            start.Visible = V1;
            exit.Visible = V1;
            electro.Visible = V;
            speaker.Visible = V;
            dialog.Text = "Добро пожаловать в Московский Политех, юный абитуриент.\n" +
                "Хочешь зайти внутрь, и посмотреть вуз изнутри?";
            yes.Visible = V;
            no.Visible = V;
        }

        private void speaker_MouseHover(object sender, EventArgs e)
        {
            speaker.Visible = V;
            wowow.Visible = V;
        }
        private void speaker_MouseLeave(object sender, EventArgs e)
        {
            wowow.Visible = V1;
            wowow2.Visible = V1;
        }

        private void no_Click(object sender, EventArgs e)
        {
            if (non == 0)
            {
                dialog.Text = "Эта кнопка придумана для того, что бы  \n" +
                    "пользователь думал, что у него есть выбор. \n" +
                    "Я уже замерз, пойдём внутрь.";
            }
            if (non > 0 && electro.Visible == true)
            {
                dialog.Text = "Сколько не жми, это бесполезно.";
            }
            non++;
        }

        private void yes_Click(object sender, EventArgs e)
        {
            electro.Visible = V1;
            conf.Visible = V;
            dialog.Text = "Отлично. Это конференц-зал. \n" +
                "Здесь проходят конференции.";
            no.Visible = V1;
            yes.Visible = V1;
            wau.Visible = V;
        }

        private void wau_Click(object sender, EventArgs e)
        {
            conf.Visible = V1;
            coolroom.Visible = V;
            wau.Visible = V1;
            dialog.Text = "В этой аудитории происходит что-то умное, \n" +
                "не будем мешать.";
            ladno.Visible = V;
        }

        private void ladno_Click(object sender, EventArgs e)
        {
            coolroom.Visible = V1;
            dddmod.Visible = V;
            dialog.Text = "В этой аудитории видимо происходит \n" +
                "какое-то 3D-моделирование.";
            ladno.Visible = V1;
            endec.Visible = V;
            more.Visible = V;

        }

        private void more_Click(object sender, EventArgs e)
        {
            dddmod.Visible = V1;
            dialog.Text = "К сожалению наша мини-экскурсия подошла к концу. \n" +
                "Может быть желаете пройти викторину?";
            endec.Visible = V1;
            more.Visible = V1;
            vicyes.Visible = V;
            vicno.Visible = V;
        }

        private void endec_Click(object sender, EventArgs e)
        {
            dddmod.Visible = V1;
            dialog.Text = "Да, конечно. Теперь викторину?";
            endec.Visible = V1;
            more.Visible = V1;
            vicyes.Visible = V;
            vicno.Visible = V;
        }

        private void vicno_Click(object sender, EventArgs e)
        {
            dialog.Text = "Ну тогда до скорой встречи, абитуриент. \n" +
                "Я уверен, что мы увидимся.";
            vicyes.Visible = V1;
            vicno.Visible = V1;
            poka.Visible = V;
        }

        private void vicyes_Click(object sender, EventArgs e)
        {
            speakervic.Visible = V;
            speaker.Visible = V1;
            dialog.Visible = V1;
            vicyes.Visible = V1;
            vicno.Visible = V1;
            question.Visible = V;
            question.Text = "Первый вопрос: \n" +
                "Как вы относитесь к нашей власти?";
            polojitelno.Visible = V;
            otricatelno.Visible = V;
        }

        private void poka_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void polojitelno_Click(object sender, EventArgs e)
        {
            question.Text = "Отлично, хороший ответ. \n" +
                "Второй вопрос: \n" +
                "Сколько локаций мы прошли сегодня?";
            two.Visible = V;
            three.Visible = V;
            four.Visible = V;
            five.Visible = V;
            nocount.Visible = V;
            polojitelno.Visible = V1;
            otricatelno.Visible = V1;
            demo.Visible = V1;

        }

        private void otricatelno_Click(object sender, EventArgs e)
        {
            demo.Text = "Давайте не забывать \n" +
                "про демократию.";
            otricatelno.Visible = V1;
        }

        private void two_Click(object sender, EventArgs e)
        {
            question.Text = "Мимо";
            two.Visible = V1;
            three.Visible = V1;
            four.Visible = V1;
            five.Visible = V1;
            nocount.Visible = V1;
            back.Visible = V;
        }

        private void back_Click(object sender, EventArgs e)
        {
            question.Text = "Второй вопрос: \n" +
            "Сколько локаций мы прошли сегодня?";
            two.Visible = V;
            three.Visible = V;
            four.Visible = V;
            five.Visible = V;
            nocount.Visible = V;
            back.Visible = V1;
        }

        private void three_Click(object sender, EventArgs e)
        {
            question.Text = "Этот ответ не является верным.";
            two.Visible = V1;
            three.Visible = V1;
            four.Visible = V1;
            five.Visible = V1;
            nocount.Visible = V1;
            back.Visible = V;
        }

        private void four_Click(object sender, EventArgs e)
        {
            question.Text = "Я не считал, но вроде то.";
            two.Visible = V1;
            three.Visible = V1;
            four.Visible = V1;
            five.Visible = V1;
            nocount.Visible = V1;
            next.Visible = V;
        }

        private void five_Click(object sender, EventArgs e)
        {
            question.Text = "Вроде меньше..";
            two.Visible = V1;
            three.Visible = V1;
            four.Visible = V1;
            five.Visible = V1;
            nocount.Visible = V1;
            back.Visible = V;
        }

        private void nocount_Click(object sender, EventArgs e)
        {
            question.Text = "Я тоже. Но я знаю. 4.";
            two.Visible = V1;
            three.Visible = V1;
            four.Visible = V1;
            five.Visible = V1;
            nocount.Visible = V1;
            back.Visible = V;
        }

        private void next_Click(object sender, EventArgs e)
        {
            question.Text = "Ладно, хорошего по немногу. \n" +
                "И этот проект в дальнейшем\n скорее всего" +
                "будет приобретать\n все новые и новые фичи.\n" +
                "Так что, оставайтесь с нами.\n До скорой встречи!";
            next.Visible = V1;
            byebye.Visible = V;
        }

        private void byebye_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
