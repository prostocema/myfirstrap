﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SimSimon
{
    public partial class Form1 : Form
    {
        int charIndex = 0;
        string text = "Добро пожаловать в симулятор Симона!!";
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            charIndex = 0;
            label1.Text = string.Empty;
            Thread t = new Thread(new ThreadStart(this.TypewriteText));
            t.Start();
        }
        private void TypewriteText()
        {
            while (charIndex < text.Length)
            {
                Thread.Sleep(60);
                label1.Invoke(new Action(() =>
                {
                    label1.Text += text[charIndex];
                }));
                charIndex++;
            }
        }
    }
}
