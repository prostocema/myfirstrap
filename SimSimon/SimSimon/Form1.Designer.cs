﻿namespace SimSimon
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.startgame = new System.Windows.Forms.PictureBox();
            this.startgame2 = new System.Windows.Forms.PictureBox();
            this.checkpc = new System.Windows.Forms.PictureBox();
            this.checkwindow = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.startgame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startgame2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkpc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkwindow)).BeginInit();
            this.SuspendLayout();
            // 
            // startgame
            // 
            this.startgame.Image = ((System.Drawing.Image)(resources.GetObject("startgame.Image")));
            this.startgame.Location = new System.Drawing.Point(0, -1);
            this.startgame.Name = "startgame";
            this.startgame.Size = new System.Drawing.Size(626, 751);
            this.startgame.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.startgame.TabIndex = 0;
            this.startgame.TabStop = false;
            // 
            // startgame2
            // 
            this.startgame2.Image = ((System.Drawing.Image)(resources.GetObject("startgame2.Image")));
            this.startgame2.Location = new System.Drawing.Point(0, -1);
            this.startgame2.Name = "startgame2";
            this.startgame2.Size = new System.Drawing.Size(626, 751);
            this.startgame2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.startgame2.TabIndex = 1;
            this.startgame2.TabStop = false;
            // 
            // checkpc
            // 
            this.checkpc.Image = ((System.Drawing.Image)(resources.GetObject("checkpc.Image")));
            this.checkpc.Location = new System.Drawing.Point(0, -1);
            this.checkpc.Name = "checkpc";
            this.checkpc.Size = new System.Drawing.Size(626, 751);
            this.checkpc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.checkpc.TabIndex = 2;
            this.checkpc.TabStop = false;
            // 
            // checkwindow
            // 
            this.checkwindow.Image = ((System.Drawing.Image)(resources.GetObject("checkwindow.Image")));
            this.checkwindow.Location = new System.Drawing.Point(0, -1);
            this.checkwindow.Name = "checkwindow";
            this.checkwindow.Size = new System.Drawing.Size(626, 751);
            this.checkwindow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.checkwindow.TabIndex = 3;
            this.checkwindow.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(624, 749);
            this.Controls.Add(this.checkwindow);
            this.Controls.Add(this.checkpc);
            this.Controls.Add(this.startgame2);
            this.Controls.Add(this.startgame);
            this.Name = "Form1";
            this.Text = "SimSimon";
            ((System.ComponentModel.ISupportInitialize)(this.startgame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startgame2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkpc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkwindow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox startgame;
        private System.Windows.Forms.PictureBox startgame2;
        private System.Windows.Forms.PictureBox checkpc;
        private System.Windows.Forms.PictureBox checkwindow;
    }
}

