﻿using System;
using System.Collections.Generic;

namespace LabDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>(5);
            x.Add(1, "Bryansk");
            x.Add(3, "Tula");
            x.Add(2, "Samara");
            x.Add(4, "Kaluga");
            x.Add(5, "Pskov");

            foreach (KeyValuePair<int, string> keyValue in x)
            {
                Console.WriteLine(keyValue.Key + " - " + keyValue.Value);
            }
            string contry = x[4];
            x[4] = "Spain";
            x.Remove(2);

            Console.WriteLine("-------");

            foreach (KeyValuePair<int, string> keyValue in x)
            {
                Console.WriteLine(keyValue.Key + " - " + keyValue.Value);
            }
        }
    }
}
