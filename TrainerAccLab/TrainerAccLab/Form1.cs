﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrainerAccLab
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            g = new Games();
            g.Change += Event_Change;
            g.DoReset();

            yes.Click += (sender, e) => g.DoAnswer(true);
            no.Click += (sender, e) => g.DoAnswer(false);
        }

        private void Event_Change(object sender, EventArgs e)
        {
            laCorrect.Text = String.Format("Верно = {0} ", g.CountCorrect.ToString());
            laWrong.Text = String.Format("He верно = {0}", g.CountWrong.ToString());
            laCode.Text = g.CodeText;
        }
    }
}
