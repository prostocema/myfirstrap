﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainerAccLab
{
    class Games
    {
        public int CountCorrect { get; protected set; }
        public int CountWrong { get; protected set; }
        public bool AnswerCorrect { get; protected set; }
        public string CodeText { get; protected set; }

        public event EventHadler Change;

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }

        public void DoContinue()
        {
            Random rnd = new Random();
            int xValue1 = rnd.Next(20);
            int xValue2 = rnd.Next(20);
            int xResult = xValue1 + xValue2;
            int xResultNew;
            int xSing;
            if (rnd.Next(2) == 1)
            {
                xResultNew = xResult;
            }
            else
            {
                if (rnd.Next(2) == 1)
                    xSing = 1;
                else
                    xSing = -1;
                xResultNew = xResult + (rnd.Next(7) * xSing);
            }
            AnswerCorrect = (xResult == xResultNew);
            CodeText = String.Format("{0} + {1} = {2}", xValue1, xValue2, xResultNew);
            if (Change != null)
                Change(this, EventArgs.Empty);

            public void DoAnswer(bool v)
            {

                if (v == AnswerCorrect)
                    CountCorrect++;
                else
                    CountWrong++;
                DoContinue();
            }



        }
    }
}
