﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabPaint
{
    public partial class fm : Form
    {
        private bool isPressed;
        private Bitmap b;

        public fm()
        {
            InitializeComponent();

            b = new Bitmap(paimage.Width, paimage.Height);
            paimage.MouseDown += paimage_MouseDown;
            paimage.MouseUp += paimage_MouseUp;
            paimage.MouseMove += paimage_MouseMove;
            paimage.Paint += paimage_Paint;

        }

        private void paimage_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                isPressed = true;
        }

        private void paimage_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false;
        }

        private void paimage_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isPressed) return;
            var g = Graphics.FromImage(b);
            g.DrawLine(new Pen(Color.Yellow, 5), e.X - 10, e.Y - 10, e.X + 10, e.Y + 10);
            g.DrawLine(new Pen(Color.Yellow, 5), e.X - 10, e.Y + 10, e.X + 10, e.Y - 10);
            g.Dispose();
            paimage.CreateGraphics().DrawImage(b, new Point(0, 0));
        }

        private void paimage_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(b, new Point(0, 0));
        }
    }
}
