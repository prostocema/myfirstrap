﻿namespace krestzero
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.restart = new System.Windows.Forms.Button();
            this.rules = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.rule3 = new System.Windows.Forms.Label();
            this.anek = new System.Windows.Forms.Button();
            this.left = new System.Windows.Forms.Button();
            this.right = new System.Windows.Forms.Button();
            this.chet = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.flip = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // restart
            // 
            this.restart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.restart.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restart.Location = new System.Drawing.Point(479, 17);
            this.restart.Name = "restart";
            this.restart.Size = new System.Drawing.Size(105, 29);
            this.restart.TabIndex = 9;
            this.restart.Text = "Сброс";
            this.restart.UseVisualStyleBackColor = false;
            this.restart.Click += new System.EventHandler(this.restart_Click);
            // 
            // rules
            // 
            this.rules.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.rules.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rules.Location = new System.Drawing.Point(590, 17);
            this.rules.Name = "rules";
            this.rules.Size = new System.Drawing.Size(105, 29);
            this.rules.TabIndex = 10;
            this.rules.Text = "Правила";
            this.rules.UseVisualStyleBackColor = false;
            this.rules.Click += new System.EventHandler(this.rules_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(701, 17);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(105, 29);
            this.button2.TabIndex = 11;
            this.button2.Text = "Выйти";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // rule3
            // 
            this.rule3.AutoSize = true;
            this.rule3.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rule3.ForeColor = System.Drawing.Color.White;
            this.rule3.Location = new System.Drawing.Point(476, 124);
            this.rule3.Name = "rule3";
            this.rule3.Size = new System.Drawing.Size(0, 16);
            this.rule3.TabIndex = 12;
            // 
            // anek
            // 
            this.anek.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.anek.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.anek.Location = new System.Drawing.Point(479, 53);
            this.anek.Name = "anek";
            this.anek.Size = new System.Drawing.Size(327, 31);
            this.anek.TabIndex = 13;
            this.anek.Text = "Задуматься...";
            this.anek.UseVisualStyleBackColor = false;
            this.anek.Click += new System.EventHandler(this.anek_Click);
            // 
            // left
            // 
            this.left.Location = new System.Drawing.Point(479, 91);
            this.left.Name = "left";
            this.left.Size = new System.Drawing.Size(75, 23);
            this.left.TabIndex = 14;
            this.left.Text = "←←←";
            this.left.UseVisualStyleBackColor = true;
            this.left.Click += new System.EventHandler(this.left_Click);
            // 
            // right
            // 
            this.right.Location = new System.Drawing.Point(731, 91);
            this.right.Name = "right";
            this.right.Size = new System.Drawing.Size(75, 23);
            this.right.TabIndex = 15;
            this.right.Text = "→→→";
            this.right.UseVisualStyleBackColor = true;
            this.right.Click += new System.EventHandler(this.right_Click);
            // 
            // chet
            // 
            this.chet.AutoSize = true;
            this.chet.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chet.ForeColor = System.Drawing.Color.White;
            this.chet.Location = new System.Drawing.Point(616, 96);
            this.chet.Name = "chet";
            this.chet.Size = new System.Drawing.Size(0, 14);
            this.chet.TabIndex = 16;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Blue;
            this.button1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Snow;
            this.button1.Location = new System.Drawing.Point(712, 395);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 38);
            this.button1.TabIndex = 17;
            this.button1.Text = "Подбросить монетку";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // flip
            // 
            this.flip.AutoSize = true;
            this.flip.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.flip.ForeColor = System.Drawing.Color.White;
            this.flip.Location = new System.Drawing.Point(728, 378);
            this.flip.Name = "flip";
            this.flip.Size = new System.Drawing.Size(0, 14);
            this.flip.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(812, 445);
            this.Controls.Add(this.flip);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chet);
            this.Controls.Add(this.right);
            this.Controls.Add(this.left);
            this.Controls.Add(this.anek);
            this.Controls.Add(this.rule3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.rules);
            this.Controls.Add(this.restart);
            this.Name = "Form1";
            this.Text = "Xzero";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button restart;
        private System.Windows.Forms.Button rules;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label rule3;
        private System.Windows.Forms.Button anek;
        private System.Windows.Forms.Button left;
        private System.Windows.Forms.Button right;
        private System.Windows.Forms.Label chet;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label flip;
    }
}

