﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace krestzero
{
    public partial class Form1 : Form
    {
      //  private int x = 12, y = 12;
        private Button[,] buttons = new Button[3, 3];
        private int player;
        private string[] shrifts = new string[4] {"Tempus Sans ITC", "Monotype Corsiva", "MS Reference Sans Serif", "NSimSun" };
        private string[] anekdos = new string[6] {"Прикажите слуге не слушаться Вас. \nНе слушаясь Вас, он ослушается приказа, \nтак как он исполняет его, слушаясь Вас.",
            "История учит человека тому, \nчто человек ничему не учится \nиз истории",
            "Почему у человека есть \nмолочные зубы, а не сливочные?", "Марс — единственная планета \nво Вселенной, населённая исключительно \nроботами.",
            "Уборка — это, по сути, перенесение грязи \nс одних вещей на другие!", ""};
        int count = 0;
        Random rnd = new Random();
        public Form1()
        {
            InitializeComponent();
            player = 1;
            for (int i = 0; i < buttons.Length / 3; i++)
            {
                for (int j = 0; j < buttons.Length / 3; j++)
                {
                    buttons[i, j] = new Button();
                    buttons[i, j].Size = new Size(150, 135);
                }
            }
            setButtons();
        }
        private void setButtons()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    buttons[i, j].Location = new Point(17 + 154 * j, 17 + 138 * i);
                    buttons[i, j].Click += button1_Click;
                    buttons[i, j].Font = new Font(shrifts[rnd.Next(4)], 76);
                    buttons[i, j].BackColor = Color.Gray;
                    this.Controls.Add(buttons[i, j]);
                    //buttons[i, j].Visible = false;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch(player)
            {
                case 1:
                    sender.GetType().GetProperty("Text").SetValue(sender, "X");
                    player = 0;
                    break;
                case 0:
                    sender.GetType().GetProperty("Text").SetValue(sender, "O");
                    player = 1;
                    break;
            }
            sender.GetType().GetProperty("Enabled").SetValue(sender, false);
            check();
            // MessageBox.Show(sender.GetType().GetProperty("Name").GetValue(sender).ToString());

        }
        private void check()
        {
            if (buttons[0,0].Text == buttons[0, 1].Text && buttons[0, 1].Text == buttons[0, 2].Text)
            {
                if (buttons[1, 0].Text != "")
                {
                    buttons[0, 0].BackColor = Color.Green;
                    buttons[0, 1].BackColor = Color.Green;
                    buttons[0, 2].BackColor = Color.Green;
                }
            }
            if (buttons[1, 0].Text == buttons[1, 1].Text && buttons[1, 1].Text == buttons[1, 2].Text)
            {
                if (buttons[1, 0].Text != "")
                {
                    buttons[1, 0].BackColor = Color.Green;
                    buttons[1, 1].BackColor = Color.Green;
                    buttons[1, 2].BackColor = Color.Green;
                }
            }
            if (buttons[2, 0].Text == buttons[2, 1].Text && buttons[2, 1].Text == buttons[2, 2].Text)
                {
                if (buttons[2, 0].Text != "")
                {
                    buttons[2, 0].BackColor = Color.Green;
                    buttons[2, 1].BackColor = Color.Green;
                    buttons[2, 2].BackColor = Color.Green;
                }
            }
            if (buttons[0, 0].Text == buttons[1, 0].Text && buttons[1, 0].Text == buttons[2, 0].Text)
            {
                if (buttons[0, 0].Text != "")
                {
                    buttons[0, 0].BackColor = Color.Green;
                    buttons[1, 0].BackColor = Color.Green;
                    buttons[2, 0].BackColor = Color.Green;
                }
            }
            if (buttons[0, 1].Text == buttons[1, 1].Text && buttons[1, 1].Text == buttons[2, 1].Text)
            {
                if (buttons[0, 1].Text != "")
                {
                    buttons[0, 1].BackColor = Color.Green;
                    buttons[1, 1].BackColor = Color.Green;
                    buttons[2, 1].BackColor = Color.Green;
                }
            }
            if (buttons[0, 2].Text == buttons[1, 2].Text && buttons[1, 2].Text == buttons[2, 2].Text)
            {
                if (buttons[0, 2].Text != "")
                {
                    buttons[0, 2].BackColor = Color.Green;
                    buttons[1, 2].BackColor = Color.Green;
                    buttons[2, 2].BackColor = Color.Green;
                }
            }
            if (buttons[0, 0].Text == buttons[1, 1].Text && buttons[1, 1].Text == buttons[2, 2].Text)
            {
                if (buttons[0, 0].Text != "")
                {
                    buttons[0, 0].BackColor = Color.Green;
                    buttons[1, 1].BackColor = Color.Green;
                    buttons[2, 2].BackColor = Color.Green;
                }
            }
            if (buttons[0, 2].Text == buttons[1, 1].Text && buttons[1, 1].Text == buttons[2, 0].Text)
            {
                if (buttons[0, 2].Text != "")
                {
                    buttons[0, 2].BackColor = Color.Green;
                    buttons[1, 1].BackColor = Color.Green;
                    buttons[2, 0].BackColor = Color.Green;
                }
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rules_Click(object sender, EventArgs e)
        {
            rule3.Text = "Правила просты:\n" +
            "1. Занимай территорию.\n" +
            "2. Не дай противнику построить стену от \n одного края доски до другого. \n" +
            "3. Не сдавайся и получай удовольствие.";
        }

        private void restart_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    buttons[i, j].Text = "";
                    buttons[i, j].Enabled = true;
                    buttons[i, j].BackColor = Color.Gray;
                }
            }
        }

        private void anek_Click(object sender, EventArgs e)
        {
            rule3.Text = anekdos[count];
            chet.Text = count.ToString() + "/4";
        }

        private void left_Click(object sender, EventArgs e)
        {
            if (count > 0)
            {
                rule3.Text = anekdos[count - 1];
                count--;
            }
            if (count == 0)
            {
                count = 4;
                rule3.Text = anekdos[4];
            }
            chet.Text = count + "/4";
        }

        private void right_Click(object sender, EventArgs e)
        {
            if (count < 5)
            {
                rule3.Text = anekdos[count + 1];
                count++;
            }
            if (count == 5)
            {
                count = 0;
                rule3.Text = anekdos[0];
            }
            chet.Text = count + "/4";
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            int fliper = rnd.Next(2);
            if (fliper == 1)
                flip.Text = "Решка";
            else
                flip.Text = "Орёл";
            
        }
    }
}
